package examen;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import static java.lang.Thread.sleep;
import javax.swing.JLabel;

public class Frame extends javax.swing.JFrame implements Runnable, KeyListener {

    private int cont = 0;
    private JLabel bala[] = new JLabel[100];

    public Frame() {
        initComponents();
        setFocusable(true);
        addKeyListener(this);

        Test();
    }

    public void Test() {
        Thread roca1 = new Thread(this);
        roca1.setName("Rock1");
        roca1.start();
        /*Thread roca2 = new Thread(this);
        roca2.setName("Rock2");
        roca2.start();
        Thread roca3 = new Thread(this);
        roca3.setName("Rock3");
        roca3.start();
        Thread roca4 = new Thread(this);
        roca4.setName("Rock4");
        roca4.start();
        Thread roca5 = new Thread(this);
        roca5.setName("Rock5");
        roca5.start();*/
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        roca1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Space Invaders");

        roca1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/examen/roca.jpg"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(roca1)
                .addGap(0, 544, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(roca1)
                .addGap(0, 335, Short.MAX_VALUE))
        );

        getAccessibleContext().setAccessibleName("Space Invaders");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel roca1;
    // End of variables declaration//GEN-END:variables

    @Override
    public void run() {

        try {

            while (true) {
                sleep((int) (Math.random() * 1000));

                if (Thread.currentThread().getName().equals("Rock1")) {

                    roca1.setLocation(roca1.getLocation().x + 10, roca1.getLocation().y);
                    if (roca1.getX() > 590) {
                        roca1.setLocation(roca1.getLocation().x = 0, roca1.getLocation().y = 100);
                        
                    }

                }

            }

        } catch (Exception e) {
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent ke) {

        if (ke.getKeyChar() == 'k') {
            CreateLabel();
            cont++;
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void CreateLabel() {

        new Thread() {
            /* public void run() {
                bala[cont] = new JLabel(">");
                bala[cont].setIcon(new javax.swing.ImageIcon(getClass().getResource("/examenhilos/bala.jpg")));
                // bala[cont].setBounds(nave.getX(), nave.getY(), 10, 10);
                add(bala[cont]);
                for (int i = 0; i < 100; i++) {
                    bala[cont].setLocation(bala[cont].getX() + 10, bala[cont].getY());
                    try {
                        sleep(50);
                    } catch (Exception e) {
                    }

                }
            }*/
        }.start();

    }

}
