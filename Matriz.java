package Ejercicios;

public class Matriz {
	int matri1[][] = new int[8][8];

	public void Llenar() {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				matri1[i][0] = 1;
			}
		}

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (i % 2 == 0)
					matri1[i][1] = 2;
				else
					matri1[i][1] = 1;
			}
		}

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				matri1[i][2] = 2;
			}
		}

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				matri1[i][3] = 3;
			}
		}

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (i % 2 == 0)
					matri1[i][4] = 3;
				else
					matri1[i][4] = 4;
			}
		}

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				matri1[i][5] = 4;
			}
		}

		int cont = 1;
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (cont % 3 == 0) {
					matri1[i][6] = 2;
					cont = 0;
				} else {
					matri1[i][6] = 3;
				}
				cont++;
			}
		}

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (i == 0) {
					matri1[0][7] = 3;

				} else {
					if (i % 3 == 0) {
						matri1[i][7] = 3;
					} else {
						matri1[i][7] = 2;
					}
				}

			}
		}

		matri1[7][7] = 0;

	}

	public void Mostrar() {
		for (int i = 0; i < 8; i++) {
			System.out.print("");

			for (int j = 0; j < 8; j++) {
				System.out.print(matri1[i][j]);

				if (j != matri1[i].length - 1)
					System.out.print("\t");
			}

			System.out.println();
			System.out.println();

		}

	}

	public static void main(String args[]) {
		Matriz obj = new Matriz();
		obj.Llenar();
		obj.Mostrar();
	}
}
