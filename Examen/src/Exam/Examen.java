package Exam;

import javax.swing.*;

public class Examen {

    private static FrameP frame1;

    public static void main(String[] args) {
        frame1 = new FrameP();
        frame1.setVisible(true);
    }

    public static void Fibo(int lim) {
        int num1 = 0;
        int num2 = 1;
        int num3;

        while (num1 + num2 <= lim) {
            num3 = num1;
            num1 = num2;
            num2 = num1 + num3;

        }
        Fibonacci.CalcularForm(num2);
    }

    public static void Capi(String cad) {
        int num1, num2;
        num1 = Integer.parseInt(cad.substring(0,1));
        num2 = Integer.parseInt(cad.substring(1,2));
        if (num1 == num2) {
            JOptionPane.showMessageDialog(null, "EL NUMERO ES CORRECTO", "ATENCION", JOptionPane.WARNING_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "NUMERO INCORRECTO", "ERROR", JOptionPane.WARNING_MESSAGE);
        }
    }

}
