package Ejercicios;

import java.util.Scanner;

public class FibonacciRecursivo {
	int n;
	String res;
	Scanner obl = new Scanner(System.in);

	public void Leer() {
		System.out.println("Escribe el l�mite de la serie:");
		n = obl.nextInt();
	}

	public int Calcular(int x) {
		if (x == 1 || x == 2) {
			return 1;
		} else {
			return Calcular(x - 1) + Calcular(x - 2);
		}
	}

	public void Mostrar() {
		System.out.println("La sucesion fibonacci de " + n + " es: " + this.Calcular(n));
	}

	public static void main(String arg[]) {
		System.out.println("***SERIE DE FIBONACCI (RECURSIVO)***");
		FibonacciRecursivo obj = new FibonacciRecursivo();
		obj.Leer();
		obj.Mostrar();
	}
}
