package examenhilos;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import static java.lang.Thread.sleep;
import javax.swing.JLabel;

public class Frame extends javax.swing.JFrame implements Runnable, KeyListener {

    private int cont = 0;
    private JLabel bala[] = new JLabel[100];

    public Frame() {
        initComponents();
        setFocusable(true);
        addKeyListener(this);

        Test();
    }

    public void Test() {
        Thread roca1 = new Thread(this);
        roca1.setName("Rock1");
        roca1.start();
        Thread roca2 = new Thread(this);
        roca2.setName("Rock2");
        roca2.start();
        Thread roca3 = new Thread(this);
        roca3.setName("Rock3");
        roca3.start();
        Thread roca4 = new Thread(this);
        roca4.setName("Rock4");
        roca4.start();
        Thread roca5 = new Thread(this);
        roca5.setName("Rock5");
        roca5.start();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        nave = new javax.swing.JLabel();
        roca1 = new javax.swing.JLabel();
        roca2 = new javax.swing.JLabel();
        roca3 = new javax.swing.JLabel();
        roca4 = new javax.swing.JLabel();
        roca5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("NAVES GAME");

        nave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/examenhilos/nave.png"))); // NOI18N

        roca1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/examenhilos/roca.jpg"))); // NOI18N

        roca2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/examenhilos/roca.jpg"))); // NOI18N

        roca3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/examenhilos/roca.jpg"))); // NOI18N

        roca4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/examenhilos/roca.jpg"))); // NOI18N

        roca5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/examenhilos/roca.jpg"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(nave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(roca3)
                    .addComponent(roca5))
                .addGap(18, 18, 18))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 456, Short.MAX_VALUE)
                        .addComponent(roca2))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(roca4)))
                .addGap(97, 97, 97))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(roca1)
                .addGap(26, 26, 26))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(roca1)
                .addGap(8, 8, 8)
                .addComponent(roca2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(roca3)
                        .addGap(46, 46, 46))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(nave, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)))
                .addComponent(roca4)
                .addGap(12, 12, 12)
                .addComponent(roca5)
                .addGap(24, 24, 24))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel nave;
    private javax.swing.JLabel roca1;
    private javax.swing.JLabel roca2;
    private javax.swing.JLabel roca3;
    private javax.swing.JLabel roca4;
    private javax.swing.JLabel roca5;
    // End of variables declaration//GEN-END:variables

    @Override
    public void run() {

        try {

            while (true) {
                sleep((int) (Math.random() * 1000));

                if (Thread.currentThread().getName().equals("Rock1")) {

                    roca1.setLocation(roca1.getLocation().x, roca1.getLocation().y + 10);
                    if (roca1.getY() > 380) {
                        roca1.setLocation(roca1.getLocation().x, roca1.getLocation().y = 0);
                    }

                    roca2.setLocation(roca2.getLocation().x, roca2.getLocation().y + 14);
                    if (roca2.getY() > 380) {
                        roca2.setLocation(roca2.getLocation().x, roca2.getLocation().y = 0);
                    }

                    roca3.setLocation(roca3.getLocation().x, roca3.getLocation().y + 10);
                    if (roca3.getY() > 380) {
                        roca3.setLocation(roca3.getLocation().x, roca3.getLocation().y = 0);
                    }

                    roca4.setLocation(roca4.getLocation().x, roca4.getLocation().y + 15);
                    if (roca4.getY() > 380) {
                        roca4.setLocation(roca4.getLocation().x, roca4.getLocation().y = 0);
                    }

                    roca5.setLocation(roca5.getLocation().x, roca5.getLocation().y + 6);
                    if (roca5.getY() > 380) {
                        roca5.setLocation(roca5.getLocation().x, roca5.getLocation().y = 0);
                    }

                    // jl.add(l);//.setLocation(  );
                }

            }

        } catch (Exception e) {
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent ke) {

        if (ke.getKeyChar() == 'k') {
            CreateLabel();
            cont++;
        }

        if (ke.getExtendedKeyCode() == KeyEvent.VK_UP) {
            nave.setLocation(nave.getX(), nave.getY() - 10);
        }
        if (ke.getExtendedKeyCode() == KeyEvent.VK_DOWN) {
            nave.setLocation(nave.getX(), nave.getY() + 10);
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void CreateLabel() {

        new Thread() {
            public void run() {
                bala[cont] = new JLabel(">");
                bala[cont].setIcon(new javax.swing.ImageIcon(getClass().getResource("/examenhilos/bala.jpg")));
                bala[cont].setBounds(nave.getX(), nave.getY(), 10, 10);
                add(bala[cont]);
                for (int i = 0; i < 100; i++) {
                    bala[cont].setLocation(bala[cont].getX() + 10, bala[cont].getY());
                    try {
                        sleep(50);
                    } catch (Exception e) {
                    }

                }
            }
        }.start();

    }

}
