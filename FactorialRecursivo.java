package Ejercicios;

import java.util.Scanner;

public class FactorialRecursivo {
	Scanner obl = new Scanner(System.in);
	int n;

	public void Leer() {
		System.out.println("Escribe el n�mero del cual quieres calcular su factorial:");
		n = obl.nextInt();
	}

	public int Calcular(int x) {
		if (x < 0) {
			return 0;
		} else {
			if (x == 0) {
				return 1;
			} else {
				return x * Calcular(x - 1);
			}
		}
	}

	public void Mostrar() {
		String res;
		System.out.println("El factorial del numero " + n + " es: " + this.Calcular(n));
	}

	public static void main(String arg[]) {
		FactorialRecursivo obj = new FactorialRecursivo();
		System.out.println("***FACTORIAL DE UN NUMERO (RECURSIVIDAD)***");
		obj.Leer();
		obj.Mostrar();
	}
}
