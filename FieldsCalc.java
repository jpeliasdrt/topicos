package Ejercicios;

import javax.swing.*;
import java.awt.event.*;

public class FieldsCalc extends JFrame implements ActionListener {
	private JTextField textfield1, textfield2;
	private JButton boton1, boton2, boton3, boton4;

	public FieldsCalc() {
		setLayout(null);
		
		textfield1 = new JTextField();
		textfield1.setBounds(10, 10, 150, 30);
		add(textfield1);
		
		textfield2 = new JTextField();
		textfield2.setBounds(10, 45, 150, 30);
		add(textfield2);
		
		boton1 = new JButton("Sumar");
		boton1.setBounds(10, 90, 100, 30);
		add(boton1);
		boton1.addActionListener(this);

		boton2 = new JButton("Restar");
		boton2.setBounds(10, 130, 100, 30);
		add(boton2);
		boton2.addActionListener(this);

		boton3 = new JButton("Multiplicar");
		boton3.setBounds(130, 90, 100, 30);
		add(boton3);
		boton3.addActionListener(this);

		boton4 = new JButton("Dividir");
		boton4.setBounds(130, 130, 100, 30);
		add(boton4);
		boton4.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == boton1) {
			String cad1 = textfield1.getText();
			String cad2 = textfield2.getText();
			int x1 = Integer.parseInt(cad1);
			int x2 = Integer.parseInt(cad2);
			int suma = x1 + x2;
			String total = String.valueOf(suma);
			setTitle(total);
		}

		if (e.getSource() == boton2) {
			String cad1 = textfield1.getText();
			String cad2 = textfield2.getText();
			int x1 = Integer.parseInt(cad1);
			int x2 = Integer.parseInt(cad2);
			int suma = x1 - x2;
			String total = String.valueOf(suma);
			setTitle(total);
		}

		if (e.getSource() == boton3) {
			String cad1 = textfield1.getText();
			String cad2 = textfield2.getText();
			int x1 = Integer.parseInt(cad1);
			int x2 = Integer.parseInt(cad2);
			int suma = x1 * x2;
			String total = String.valueOf(suma);
			setTitle(total);
		}

		if (e.getSource() == boton4) {
			String cad1 = textfield1.getText();
			String cad2 = textfield2.getText();
			int x1 = Integer.parseInt(cad1);
			int x2 = Integer.parseInt(cad2);
			int suma = x1 / x2;
			String total = String.valueOf(suma);
			setTitle(total);
		}
	}

	public static void main(String[] ar) {
		FieldsCalc field = new FieldsCalc();
		field.setBounds(0, 0, 300, 300);
		field.setVisible(true);
	}

}