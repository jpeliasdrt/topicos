package Ejercicios;

import javax.swing.JOptionPane;

public class FactorialEscritorio {
	int res = 1, tam;

	/*
	 * public void Leer() { String tama�o =
	 * JOptionPane.showInputDialog("Dame un numero:"); }
	 */

	public void Calcular() {

		String tama = JOptionPane.showInputDialog("Dame un numero: ");
		int tam = Integer.parseInt(tama);

		for (int x = 0; x <= tam; x++) {
			if (x == 0) {
				x = 1;
			}

			res = res * x;
		}

		for (int x = 1; x <= tam; x++) {
			JOptionPane.showMessageDialog(null, x);
			if (x != tam)
				JOptionPane.showMessageDialog(null, " * ");
			if (x == tam)
				JOptionPane.showMessageDialog(null, " = " + res);
		}
	}

	/*
	 * public void Mostrar() { JOptionPane.showMessageDialog(null, "El valor es " +
	 * res); }
	 */

	public static void main(String args[]) {
		FactorialEscritorio objeto = new FactorialEscritorio();
		// objeto.Leer();
		objeto.Calcular();
		// objeto.Mostrar();
	}

}
