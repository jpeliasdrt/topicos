package cliente2;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Servidor {

    public static void main(String[] args) {

        ServerSocket servidor = null;
        Socket sk = null;
        DataInputStream entrada;
        DataOutputStream salida;

        //puerto de nuestro servidor
        final int PUERTO = 12345;

        try {
            //Creamos el socket del servidor
            servidor = new ServerSocket(PUERTO);
            System.out.println("Servidor iniciado");

            //Siempre estara escuchando peticiones
            while (true) {

                //Espero a que un cliente se conecte
                sk = servidor.accept();

                System.out.println("Cliente conectado");
                entrada = new DataInputStream(sk.getInputStream());
                salida = new DataOutputStream(sk.getOutputStream());

                //Leo el mensaje que me envia
                String mensaje = entrada.readUTF();

                System.out.println(mensaje);
                salida.writeUTF("¡Servidor!");
                //Cierro el socket
                sk.close();
                System.out.println("Cliente cerro conexión ");

            }

        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
