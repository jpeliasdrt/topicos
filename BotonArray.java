package Ejercicios;

import javax.swing.*;
import java.awt.event.*;

public class BotonArray extends JFrame implements ActionListener {
	private JTextField textfield1;
	JButton boton1;
	JButton botonC[] = new JButton[11];

	public void FieldsCalc() {

	}

	public BotonArray() {
		int i, x, y;

		setLayout(null);

		textfield1 = new JTextField();
		textfield1.setBounds(100, 10, 270, 30);
		add(textfield1);
		
		setLayout(null);

		for (i = 1; i < 4; i++) {
			botonC[i] = new JButton("" + i + "");
			x = 0 + i * 100;
			y = 0 + i * 10;

			botonC[i].setBounds(x, 50, 70, 40);
			add(botonC[i]);
		}

		for (i = 4; i < 7; i++) {
			botonC[i] = new JButton("" + i + "");
			x = 0 + i - 3;
			x = x * 100;
			y = 0 + i * 10;

			botonC[i].setBounds(x, 100, 70, 40);
			add(botonC[i]);
		}

		for (i = 7; i < 10; i++) {
			botonC[i] = new JButton("" + i + "");
			x = 0 + i - 6;
			x = x * 100;
			y = 0 + i * 10;

			botonC[i].setBounds(x, 150, 70, 40);
			add(botonC[i]);
		}

		for (i = 10; i < 11; i++) {
			botonC[i] = new JButton("" + 0 + "");
			x = 0 + i - 8;
			x = x * 100;
			y = 0 + i * 10;

			botonC[i].setBounds(x, 200, 70, 40);
			add(botonC[i]);
		}

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == boton1) {
			System.exit(0);
		}
	}

	public static void main(String[] ar) {
		BotonArray formulario1 = new BotonArray();
		formulario1.setBounds(0, 0, 450, 350);
		formulario1.setVisible(true);
	}

}
