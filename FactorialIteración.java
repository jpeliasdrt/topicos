package Ejercicios;

import java.util.Scanner;;

public class FactorialIteración {
	int tamaño, res = 1;
	Scanner obl = new Scanner(System.in);

	public void Leer() {
		System.out.println("***FACTORIAL DE UN NUMERO***\nEscribe el numero a calcular su factorial:");
		tamaño = obl.nextInt();
	}

	public void Calcular() {

		for (int x = 0; x <= tamaño; x++) {
			if (x == 0) {
				x = 1;
			}

			res = res * x;
		}
	}

	public void Mostrar() {
		for (int x = 1; x <= tamaño; x++) {
			System.out.print(x);
			if (x != tamaño)
				System.out.print(" * ");
			if (x == tamaño)
				System.out.print(" = " + res);

		}

	}

	public static void main(String args[]) {
		FactorialIteración fac = new FactorialIteración();
		fac.Leer();
		fac.Calcular();
		fac.Mostrar();
	}

}
