package horserace;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class Race extends Thread {

    private JLabel A;
    private RaceFrame B;

    public Race(JLabel A, RaceFrame B) {
        this.A = A;
        this.B = B;
    }

    public void run() {
        int horse1 = 0, horse2 = 0, band = 0;
        while (true) {
            try {
                sleep((int) (Math.random() * 1000));
                horse1 = B.getBlanco().getLocation().x;
                horse2 = B.getSpirit().getLocation().x;
                if (horse1 < B.getMeta().getLocation().x - 10 && horse2 < B.getMeta().getLocation().x - 10) {
                    A.setLocation(A.getLocation().x + 10, A.getLocation().y);
                    B.repaint();
                } else {
                    break;
                }
            } catch (InterruptedException e) {

            }

            if (A.getLocation().x >= B.getBarrera().getLocation().x - 10) {

                if (band == 0) {
                    JOptionPane.showMessageDialog(null, "TIRO AL BLANCO SE ACERCA A LA META");

                    band = 1;

                }
            }
            if (A.getLocation().x >= B.getMeta().getLocation().x - 10) {
                if (horse1 > horse2) {
                    JOptionPane.showMessageDialog(null, "GANO TIRO AL BLANCO");
                } else if (horse1 < horse2) {
                    JOptionPane.showMessageDialog(null, "GANO SPIRIT");
                } else {
                    JOptionPane.showMessageDialog(null, "EMPATE");
                }
            }
        }
    }
}
