package Ejercicios;

import java.util.Scanner;

public class FinbonacciIteracion {
	int num1 = 0;
	int num2 = 1;
	int num3;
	int lim;
	Scanner obl = new Scanner(System.in);

	public void Leer() {
		System.out.println("***SERIE DE FIBONACCI (ITERACIÓN)***\n Escribe el limite de la serie:");
		lim = obl.nextInt();
	}

	public void Calcular() {
		System.out.print(num1 + "\t" + num2);

		while (num1 + num2 <= lim) {
			num3 = num1;
			num1 = num2;
			num2 = num1 + num3;

			System.out.print("\t");
			System.out.print(num2);

		}
	}

	public static void main(String args[]) {
		FinbonacciIteracion obj = new FinbonacciIteracion();
		obj.Leer();
		obj.Calcular();
	}
}
