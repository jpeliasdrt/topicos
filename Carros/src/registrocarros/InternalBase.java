package registrocarros;

import javax.swing.JOptionPane;
import java.io.*;

public class InternalBase {

    private static Frame1 frames;
    private static AltaCarro framec;
    private static BuscarCarro frameb;
    private static OrdenarCarros frameo;
    private static Cars car[];
    private static int cont;

    public static void main(String[] args) {
        frames = new Frame1();
        car = new Cars[10];
        framec = new AltaCarro();
        frameb = new BuscarCarro();

        for (int i = 0; i < car.length; i++) {
            car[i] = new Cars();
        }
        cont = 0;
        comprobarArchivo();
        frames.setVisible(true);
    }

    public static void alta(int id, String marca, String modelo, String color) {
        car[cont].setID(id);
        car[cont].setMarca(marca);
        car[cont].setModelo(modelo);  //DAN ACCESO AL CONTENIDO QUE ESTÁ GUARDADO EN MEMORIA
        car[cont].setColor(color);
        cont++;
    }

    public static void Modificar(int id, String marca, String modelo, String color) {
        int band = 0;
        for (int i = 0; i < 10; i++) {
            car[i].setID(id);
            car[i].setMarca(marca);
            car[i].setModelo(modelo);  //DAN ACCESO AL CONTENIDO QUE ESTÁ GUARDADO EN MEMORIA
            car[i].setColor(color);
            
            
            
        }
    }

    public static void comprobarArchivo() {
        File f = new File("Carros");

        //System.out.println(f);
        try {
            FileInputStream carros = new FileInputStream(f);
            System.out.println(carros);
            ObjectInputStream cars = new ObjectInputStream(carros);
            car = (Cars[]) cars.readObject();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void Guardar() {
        File f = new File("Carros");
        try {
            if (!f.exists()) {
                FileOutputStream carr = new FileOutputStream(f);
                ObjectOutputStream carrs = new ObjectOutputStream(carr);
                carrs.writeObject(car);
                //carrs.flush();
                carrs.close();
                carr.close();
            } else {
                FileInputStream carros = new FileInputStream(f);
                ObjectInputStream cars = new ObjectInputStream(carros);
                car = (Cars[]) cars.readObject();
                cars.close();
                FileOutputStream carr = new FileOutputStream(f);
                ObjectOutputStream carrs = new ObjectOutputStream(carr);
                carrs.writeObject(car);
                //carrs.flush();
                carrs.close();
                carros.close();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void Mostrar() {
        String aux = "";

        for (int i = 0; i < 10; i++) {

            aux = aux + car[i].getID() + "\n" + car[i].getMarca() + "\n" + car[i].getModelo() + "\n" + car[i].getColor() + "\n";
        }
        framec.MostrarForm(aux);
    }

    public static void Buscar(String save1) {
        String resp = "";
        int band = 0;
        int num = Integer.parseInt(save1);
        for (int i = 0; i < car.length; i++) {
            if (num == car[i].getID()) {
                resp = resp + car[i].getID() + "\n" + car[i].getMarca() + "\n" + car[i].getModelo() + "\n" + car[i].getColor() + "\n";
                band = 1;
            }
        }
        if (band == 0) {
            JOptionPane.showMessageDialog(null, "NO HAY REGISTROS", "ATENCION", JOptionPane.WARNING_MESSAGE);
        }
        frameb.BuscarForm(resp);
    }

    public static void Eliminar(String delete) {
        //String resp = "";
        Cars aux = new Cars();
        int band = 0;
        int num = Integer.parseInt(delete);

        for (int i = 0; i < car.length; i++) {
            if (num == car[i].getID()) {
                band = 1;
            }
            if (band == 1) {
                car[i].setID(-1);
                car[i].setMarca("");
                car[i].setModelo("");
                car[i].setColor("");
                break;
            }
        }
        for (int i = 0; i < car.length; i++) {
            if (car[i].getID() == -1) {
                car[i] = car[i + 1];
                car[i + 1] = car[i + 2];
                break;
            }
        }

        if (band == 0) {
            JOptionPane.showMessageDialog(null, "NO EXISTE ESE REGISTRO", "ATENCION", JOptionPane.WARNING_MESSAGE);
        } else if (band == 1) {
            JOptionPane.showMessageDialog(null, "REGISTRO ELIMINADO CON EXITO", "ATENCION", JOptionPane.WARNING_MESSAGE);
        }
    }

    public static void Modificar(int id) {
        for (int i = 0; i < car.length; i++) {
            if (id == car[i].getID()) {
                JOptionPane.showMessageDialog(null, car[i].getID() + "\n" + car[i].getMarca() + "\n" + car[i].getModelo() + "\n" + car[i].getColor(), "REGISTRO A EDITAR", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    public static void Burbuja() {
        frameo.ordenarForm(car);
    }
}
