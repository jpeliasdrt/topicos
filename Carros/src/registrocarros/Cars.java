package registrocarros;

import java.io.Serializable;

public class Cars implements Serializable {

    String Marca, Modelo, Color;
    int ID;

    public Cars() {
        this.ID = 0;
        this.Marca = "";
        this.Modelo = "";
        this.Color = "";
    }

    public int getID() {
        return ID;
    }

    public void setID(int id) {
        this.ID = id;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String marca) {
        this.Marca = marca;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String modelo) {
        this.Modelo = modelo;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        this.Color = color;
    }
}
