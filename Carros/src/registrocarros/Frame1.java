package registrocarros;

public class Frame1 extends javax.swing.JFrame {

    public Frame1() {
        initComponents();
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Escritorio = new javax.swing.JDesktopPane();
        menubar = new javax.swing.JMenuBar();
        menu1 = new javax.swing.JMenu();
        PrincipalMenu = new javax.swing.JMenu();
        SubMenu1 = new javax.swing.JMenuItem();
        SubMenu2 = new javax.swing.JMenuItem();
        SubMenu3 = new javax.swing.JMenuItem();
        SubMenu4 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout EscritorioLayout = new javax.swing.GroupLayout(Escritorio);
        Escritorio.setLayout(EscritorioLayout);
        EscritorioLayout.setHorizontalGroup(
            EscritorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 560, Short.MAX_VALUE)
        );
        EscritorioLayout.setVerticalGroup(
            EscritorioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 368, Short.MAX_VALUE)
        );

        menu1.setText("File");

        PrincipalMenu.setText("Carro");

        SubMenu1.setText("ALTA");
        SubMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SubMenu1ActionPerformed(evt);
            }
        });
        PrincipalMenu.add(SubMenu1);

        SubMenu2.setText("BUSCAR");
        SubMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SubMenu2ActionPerformed(evt);
            }
        });
        PrincipalMenu.add(SubMenu2);

        SubMenu3.setText("ELIMINAR");
        SubMenu3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SubMenu3ActionPerformed(evt);
            }
        });
        PrincipalMenu.add(SubMenu3);

        SubMenu4.setText("MODIFICAR");
        SubMenu4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SubMenu4ActionPerformed(evt);
            }
        });
        PrincipalMenu.add(SubMenu4);

        menu1.add(PrincipalMenu);

        menubar.add(menu1);

        jMenu2.setText("Edit");
        menubar.add(jMenu2);

        setJMenuBar(menubar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Escritorio)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Escritorio)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SubMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SubMenu1ActionPerformed
        AltaCarro car = new AltaCarro();
        Escritorio.add(car);
        car.show();
    }//GEN-LAST:event_SubMenu1ActionPerformed

    private void SubMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SubMenu2ActionPerformed
        BuscarCarro busc = new BuscarCarro();
        Escritorio.add(busc);
        busc.show();
    }//GEN-LAST:event_SubMenu2ActionPerformed

    private void SubMenu3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SubMenu3ActionPerformed
        EliminarCarro delete = new EliminarCarro();
        Escritorio.add(delete);
        delete.show();
    }//GEN-LAST:event_SubMenu3ActionPerformed

    private void SubMenu4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SubMenu4ActionPerformed
        ModificarCarro change = new ModificarCarro();
        Escritorio.add(change);
        change.show();
    }//GEN-LAST:event_SubMenu4ActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frame1().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane Escritorio;
    private javax.swing.JMenu PrincipalMenu;
    private javax.swing.JMenuItem SubMenu1;
    private javax.swing.JMenuItem SubMenu2;
    private javax.swing.JMenuItem SubMenu3;
    private javax.swing.JMenuItem SubMenu4;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu menu1;
    private javax.swing.JMenuBar menubar;
    // End of variables declaration//GEN-END:variables
}
