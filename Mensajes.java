package Ejercicios;

import javax.swing.JOptionPane;


public class Mensajes {
	int res = 1, tam;


	public void Calcular() {

		String tama = JOptionPane.showInputDialog("Dame un numero: ");
		int tam = Integer.parseInt(tama);

		for (int x = 0; x <= tam; x++) {
			if (x == 0) {
				x = 1;
			}

			res = res * x;
		}

		// MOSTRAR
		//for (int x = 1; x <= tam; x++) {
			JOptionPane.showMessageDialog(null, res);
		//}
			/*if (x != tam)
				JOptionPane.showMessageDialog(null, " * ");
			if (x == tam)
				JOptionPane.showMessageDialog(null, " = " + res);
		}*/
		JOptionPane.showMessageDialog(null, "Mensaje dentro de la ventana",
				"Mensaje en la barra de titulo",JOptionPane.WARNING_MESSAGE);
		
		JOptionPane.showMessageDialog(null, "Mensaje dentro de la ventana",
				"Mensaje en la barra de titulo",JOptionPane.ERROR_MESSAGE);
		
		JOptionPane.showMessageDialog(null, "Mensaje dentro de la ventana",
				"Mensaje en la barra de titulo",JOptionPane.INFORMATION_MESSAGE);
		
		JOptionPane.showMessageDialog(null, "Mensaje dentro de la ventana",
				"Mensaje en la barra de titulo",JOptionPane.QUESTION_MESSAGE);
		
		JOptionPane.showMessageDialog(null, "Mensaje dentro de la ventana",
				"Mensaje en la barra de titulo",JOptionPane.PLAIN_MESSAGE);
	}

	/*
	 * public void Mostrar() { JOptionPane.showMessageDialog(null, "El valor es " +
	 * res); }
	 */

	public static void main(String args[]) {
		Mensajes objeto = new Mensajes();
		// objeto.Leer();
		objeto.Calcular();
		// objeto.Mostrar();
	}

}
